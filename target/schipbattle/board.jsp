<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Board</title>
    <link rel="stylesheet" type="text/css" href="boardstyle.css">
    <script>
        let list = [];

        function shipSelect(id) {
            console.log(id);
            document.getElementById(id).classList.toggle("shipSelected");
            if (list.includes(id)) {
                list = list.filter(item => item !== id);
            } else {
                list.push(id);
            }
            console.log(list);
        }

        function getShips() {
            document.getElementById("listparameter").value = list;
            console.log(list);
            return true;
        }
    </script>
</head>

<body>

<table border="1">
    <tr>
        <th></th>
        <th>A</th>
        <th>B</th>
        <th>C</th>
        <th>D</th>
        <th>E</th>
        <th>F</th>
        <th>G</th>
        <th>H</th>
        <th>I</th>
        <th>J</th>
    </tr>
    <c:set var="counter" value="0"/>
    <c:forEach items="${board}" begin="0" var="row">
        <tr>
            <td>${counter = counter+1}</td>
            <c:forEach items="${row}" var="cell">
<%--                <c:set var="i" value="${cell.getIdx()}"/>--%>
<%--                <c:set var="j" value="${cell.getIdy()}"/>--%>
<%--                <c:set var="id" value="${i};${j}"/>--%>
                                <c:set var="id" value="${cell.getIdx()};${cell.getIdy()}"/>

                <td>
                    <div class="${cell.getValue()}" id="${id}"
                         onclick="shipSelect('${id}')"> ${cell.getValue()} </div>
                </td>
            </c:forEach>
        </tr>
    </c:forEach>
</table>
<p>
    <%--    <form method="post">--%>
    <%--    <input type="submit" value="SUBMIT" onclick="window.location.href = '${pageContext.request.contextPath}/shipselection?list='+getShips()">SUBMIT</input>--%>
    <%--    </form>--%>

<form name="shipselection" method="post" action="shipselection" onsubmit="getShips()">
    <input type="hidden" name="list" value="" id="listparameter">
    <input type="submit" value="Submit">
    <%--    onclick="getShips()"--%>
</form>

</p>

<div id="test">TEST</div>

</body>
</html>
