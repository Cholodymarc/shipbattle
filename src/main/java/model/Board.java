package model;

public class Board {
    public static final int BOARD_SIZE = 10;
    BoardField[][] board = new BoardField[BOARD_SIZE][BOARD_SIZE];

    public Board() {
        loadEmptyBoard();
    }

    public void loadEmptyBoard() {
        for (int i = 0; i < BOARD_SIZE; i++) {
            for (int j = 0; j < BOARD_SIZE; j++) {
                board[i][j] = new BoardField(i,j, Value.EMPTY);
            }
        }
    }

    public BoardField[][] getBoardFields() {
        return board;
    }
}
