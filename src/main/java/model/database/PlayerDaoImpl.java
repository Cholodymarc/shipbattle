package model.database;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerDaoImpl implements PlayerDao {

    private static Logger logger = LoggerFactory.getLogger(PlayerDaoImpl.class);

    ConnectionFactory connectionFactory;

    public PlayerDaoImpl(ConnectionFactory connectionFactory) {
        this.connectionFactory = connectionFactory;
    }

    @Override
    public void add(Player player) {
        logger.info("ADDING NEW PLAYER TO THE REGISTERED PLAYERS TABLE");
        String query = "INSERT INTO registered_players " +
                "(email, pwd, jsession)" +
                "VALUES (?,?,?);";
        try (PreparedStatement statement = connectionFactory.getConnection().prepareStatement(query)) {
            //parameterIndex zaczyna się od 1!;
            statement.setString(1, player.getEmail());
            statement.setString(2, player.getPwd());
            statement.setString(3, player.getJsession());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Player find(String email, String pwd) {
        logger.info("PLAYERDAO FIND METHOD CALL");
        String query = "SELECT * FROM registered_players WHERE email = ? and pwd = ?;";
        try (PreparedStatement statement = connectionFactory.getConnection().prepareStatement(query)) {
            statement.setString(1, email);
            statement.setString(2, pwd);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Player extractedPlayer = new Player();
                extractedPlayer.setId(resultSet.getInt("id"));
                extractedPlayer.setEmail(resultSet.getString("email"));
                extractedPlayer.setPwd(resultSet.getString("pwd"));
                extractedPlayer.setJsession(resultSet.getString("jsession"));
                return extractedPlayer;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
