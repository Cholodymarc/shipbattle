package model.database;

public interface PlayerDao {
    void add(Player player);
    Player find(String email, String pwd);
}
