package model.database;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionFactory {
    private static Logger logger = LoggerFactory.getLogger(ConnectionFactory.class);

    private MysqlDataSource dataSource;

    public ConnectionFactory() {
        this("/database.properties");
    }

    public ConnectionFactory(String fileName){
        try {
            dataSource = new MysqlDataSource();
            dataSource.setServerName(getDataBaseProperties(fileName).getProperty("model.database.server"));
            dataSource.setDatabaseName(getDataBaseProperties(fileName).getProperty("model.database.name"));
            dataSource.setUser(getDataBaseProperties(fileName).getProperty("model.database.user"));
            dataSource.setPassword(getDataBaseProperties(fileName).getProperty("model.database.password"));
            dataSource.setPort(Integer.valueOf(getDataBaseProperties(fileName).getProperty("model.database.port")));
            dataSource.setServerTimezone("Europe/Warsaw");
            dataSource.setUseSSL(false);
            dataSource.setCharacterEncoding("UTF-8");
        } catch (SQLException e) {
            logger.error("Error during creating MysqlDataSource", e);
            throw new RuntimeException(e);
        }
    }

    private Properties getDataBaseProperties(String filename) {

        Properties properties = new Properties();
        try {
            /**
             * Pobieramy zawartość pliku za pomocą classloadera, plik musi znajdować się w katalogu ustawionym w CLASSPATH
             */
            InputStream propertiesStream = ConnectionFactory.class.getResourceAsStream(filename);
            if(propertiesStream == null) {
                throw new IllegalArgumentException("Can't find file: " + filename);
            }
            /**
             * Pobieramy dane z pliku i umieszczamy w obiekcie klasy Properties
             */
            properties.load(propertiesStream);
        } catch (IOException e) {
            logger.error("Error during fetching properties for database", e);
            return null;
        }

        return properties;
    }

    public Connection getConnection() throws SQLException {

        logger.info("Connecting to a selected database...");

        /**
         * Krok 2: Otwieramy połączenie do bazy danych
         */
        Connection connection = dataSource.getConnection();
        return connection;
    }

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        try {
            connectionFactory.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
