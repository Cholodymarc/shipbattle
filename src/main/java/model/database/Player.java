package model.database;

public class Player {
    private int id;
    private String email;
    private String pwd;
    private String jsession;

    public Player() {
    }

    public Player(String email, String pwd, String jsession) {
        this.email = email;
        this.pwd = pwd;
        this.jsession = jsession;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public String getJsession() {
        return jsession;
    }

    public void setJsession(String jsession) {
        this.jsession = jsession;
    }

    @Override
    public String toString() {
        return "Player{" +
                "email='" + email + '\'' +
                ", pwd='" + pwd + '\'' +
                ", jsession='" + jsession + '\'' +
                '}';
    }
}
