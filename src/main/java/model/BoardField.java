package model;

public class BoardField {
    private int idx;
    private int idy;
    private Value value;
    private int numberOfMasts;

    public BoardField(int idx, int idy, Value value) {
        this.idx = idx;
        this.idy = idy;
        this.value = value;
        this.numberOfMasts = 0;
    }

    public int getIdx() {
        return idx;
    }

    public int getIdy() {
        return idy;
    }

    public Value getValue() {
        return value;
    }

    public void setValue(Value value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "BoardField{" +
                "idx=" + idx +
                ", idy=" + idy +
                ", value=" + value +
                '}';
    }

}
