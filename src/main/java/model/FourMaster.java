package model;

import java.util.List;

public class FourMaster extends Ship {
    public static final int MAX_NB_OF_FOUR_MASTER = 1;
    public static int numberOfFourMaster = 0;
    Coordinates baseField;
    Coordinates field1;
    Coordinates field2;
    Coordinates field3;
    Coordinates field4;
    List<Coordinates> rimRelativeCoordinates;

    public List<Coordinates> getRimRelativeCoordinates() {
        return rimRelativeCoordinates;
    }

    public void setRimRelativeCoordinates(List<Coordinates> rimRelativeCoordinates) {
        this.rimRelativeCoordinates = rimRelativeCoordinates;
    }

    public Coordinates getField1() {
        return field1;
    }

    public void setField1(Coordinates field1) {
        this.field1 = field1;
    }

    public Coordinates getField2() {
        return field2;
    }

    public void setField2(Coordinates field2) {
        this.field2 = field2;
    }

    public Coordinates getField3() {
        return field3;
    }

    public void setField3(Coordinates field3) {
        this.field3 = field3;
    }

    public Coordinates getField4() {
        return field4;
    }

    public void setField4(Coordinates field4) {
        this.field4 = field4;
    }

    public void findFourMaster(List<String> proposedShips) {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (proposedShips.contains((i + field1.getX()) + ";" + (j + field1.getY()))
                        && proposedShips.contains(i + field2.getX() + ";" + (j + field2.getY()))
                        && proposedShips.contains(i + field3.getX() + ";" + (j + field3.getY()))
                        && proposedShips.contains(i + field4.getX() + ";" + (j + field4.getY()))) {
                    baseField = new Coordinates(i + field1.getX(), j + field1.getY());
                    System.out.println("FOUR FOUND FUN FUN FUN");
                }
            }
        }
    }

    public boolean checkRim(BoardField[][] boardFields) {
        for (Coordinates coordinates : rimRelativeCoordinates) {
            if (coordinates.getX() + baseField.getX() > 0
                    && coordinates.getX() + baseField.getX() < 10
                    && coordinates.getY() + baseField.getY() > 0
                    && coordinates.getY() + baseField.getY() < 10
                    && !boardFields[coordinates.getX() + baseField.getX()][coordinates.getY() + baseField.getY()].getValue().equals(Value.EMPTY)
            ) {
                return false;
            }
        }
        return true;
    }

    public void addFourMaster(BoardField[][] boardFields) {
        boardFields[baseField.getX()][baseField.getY()].setValue(Value.SHIP);
        boardFields[baseField.getX() + field2.getX()][baseField.getY() + field2.getY()].setValue(Value.SHIP);
        boardFields[baseField.getX() + field3.getX()][baseField.getY() + field3.getY()].setValue(Value.SHIP);
        boardFields[baseField.getX() + field4.getX()][baseField.getY() + field4.getY()].setValue(Value.SHIP);
        numberOfFourMaster++;
    }

    public void removeFromProposedShips(List<String> proposedShips) {
        String s1 = baseField.getX() + ";" + baseField.getY();
        String s2 = (baseField.getX() + field2.getX()) + ";" + (baseField.getY() + field2.getY());
        String s3 = (baseField.getX() + field3.getX()) + ";" + (baseField.getY() + field3.getY());
        String s4 = (baseField.getX() + field4.getX()) + ";" + (baseField.getY() + field4.getY());

        proposedShips.remove(s1);
        proposedShips.remove(s2);
        proposedShips.remove(s3);
        proposedShips.remove(s4);
    }
}

