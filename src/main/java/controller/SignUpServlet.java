package controller;

import model.database.ConnectionFactory;
import model.database.Player;
import model.database.PlayerDao;
import model.database.PlayerDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "SignUpServlet", value = "/signup")
public class SignUpServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("SungUpEmail");
        String pwd = req.getParameter("signUpPwd");
        String jsession = req.getSession().getId();

        Player player = new Player(email, pwd, jsession);

        PlayerDao playerDao = new PlayerDaoImpl(new ConnectionFactory());
        if (playerDao.find(player.getEmail(), player.getPwd()) == null) {
            playerDao.add(player);
        } else {
            req.setAttribute("userAlreadyExists", true);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
            dispatcher.forward(req,resp);
        }
    }
}
