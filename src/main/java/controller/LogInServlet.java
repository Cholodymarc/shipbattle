package controller;

import model.database.ConnectionFactory;
import model.database.Player;
import model.database.PlayerDao;
import model.database.PlayerDaoImpl;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "LogInServlet", value = "/login")
public class LogInServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("logInEmail");
        String pwd = req.getParameter("logInPwd");

        HttpSession session = req.getSession();

        PlayerDao playerDao = new PlayerDaoImpl(new ConnectionFactory());
        Player extractedPlayer = playerDao.find(email, pwd);
        if (extractedPlayer == null) {
            req.setAttribute("wrongCredentials", true);
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/index.jsp");
            dispatcher.forward(req,resp);
        } else {
            req.setAttribute("wrongCredentials", false);
            session.setAttribute("playerId", extractedPlayer.getId());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/start.jsp");
            dispatcher.forward(req,resp);
        }
    }
}
