package controller;

import model.Board;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "BoardServlet", value = "/board")
public class BoardServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Board board = new Board();
        req.setAttribute("board", board.getBoardFields());
        HttpSession session = req.getSession();
        session.setAttribute("board",board.getBoardFields());
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/board.jsp");
        dispatcher.forward(req, resp);
    }

}
