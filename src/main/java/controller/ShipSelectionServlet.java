package controller;

import model.Board;
import model.BoardField;
import model.Value;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebServlet(name = "ShipSelectionServlet", value = "/shipselection")
public class ShipSelectionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String[] ships = req.getParameter("list").split(",");
        System.out.println(ships.toString());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Board board = new Board();
        BoardField[][] temporaryBoard = board.getBoardFields();

        String[] ships = req.getParameter("list").split(",");
        Arrays.sort(ships);

        int x;
        int y;

        for (String ship : ships) {
            x = Integer.parseInt(String.valueOf(ship.charAt(0)));
            y = Integer.parseInt(String.valueOf(ship.charAt(2)));
            temporaryBoard[x][y].setValue(Value.TEMP_SHIP);
        }

        System.out.println("===== FOUR MASTER =======");
        validateShips(temporaryBoard, 4, 1);
        printBoard(temporaryBoard);

        System.out.println("===== THREE MASTER =======");
        validateShips(temporaryBoard, 3, 2);
        printBoard(temporaryBoard);

        System.out.println("===== TWO MASTER =======");
        validateShips(temporaryBoard, 2, 3);
        printBoard(temporaryBoard);

        System.out.println("===== ONE MASTER =======");
        validateShips(temporaryBoard, 1, 4);
        printBoard(temporaryBoard);

        HttpSession session = req.getSession();
        BoardField[][] boardFields = (BoardField[][]) session.getAttribute("board");
    }

    private void validateShips(BoardField[][] temporaryBoard, int numberOfMasters, int numberOfShips) {
        List<BoardField> shipTempFields = new ArrayList<>();
        List<BoardField> boardTempFields = new ArrayList<>();

        int shipsCounter = 0;
        int counter = 0;
        for (int i = 0; i < Board.BOARD_SIZE; i++) {
            for (int j = 0; j < Board.BOARD_SIZE; j++) {
                if (temporaryBoard[i][j].getValue() == Value.TEMP_SHIP) {
                    counter++;
                    shipTempFields.add(temporaryBoard[i][j]);
                }
                if (temporaryBoard[i][j].getValue() == Value.EMPTY && counter < numberOfMasters) {
                    counter = 0;
                    shipTempFields.clear();
                }
                if (counter == numberOfMasters) {
                    boardTempFields.addAll(shipTempFields);
                    shipsCounter++;
                    counter = 0;
                    shipTempFields.clear();
                }
            }
        }

        if (numberOfMasters > 1) {
            counter = 0;
            for (int j = 0; j < Board.BOARD_SIZE; j++) {
                for (int i = 0; i < Board.BOARD_SIZE; i++) {
                    if (temporaryBoard[i][j].getValue() == Value.TEMP_SHIP) {
                        counter++;
                        shipTempFields.add(temporaryBoard[i][j]);
                    }
                    if (temporaryBoard[i][j].getValue() == Value.EMPTY && counter < numberOfMasters) {
                        counter = 0;
                        shipTempFields.clear();
                    }
                    if (counter == numberOfMasters) {
                        boardTempFields.addAll(shipTempFields);
                        shipsCounter++;
                        counter = 0;
                        shipTempFields.clear();
                    }
                }
            }
        }

        if (shipsCounter == numberOfShips) {
            for (BoardField boardField : boardTempFields) {
                boardField.setValue(Value.SHIP);
            }
            boardTempFields.clear();
        } else {
            //inform that board was not properly filled in
            System.out.println("TOOO MANY SHIPS, found " + shipsCounter + " with " + numberOfMasters + " masters.");
        }
    }

    private void printBoard(BoardField[][] boardFields) {
        for (int i = 0; i < Board.BOARD_SIZE; i++) {
            for (int j = 0; j < Board.BOARD_SIZE; j++) {
                System.out.print(String.format("%17s", i + ":" + j + " " + boardFields[i][j].getValue()));
            }
            System.out.println();
        }
    }
}
