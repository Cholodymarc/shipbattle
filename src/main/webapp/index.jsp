<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
<h2>WELCOME TO THE BATTLE OF SHIPS!</h2>


<table>
    <tr>
        <td>
            <div>
                <c:if test="${wrongCredentials}">
                    <p>YOU PROVIDED INCORRECT EMAIL OR PASSWORD</p>
                </c:if>
                <form method="post" action="login">
                    <div class="form-group">
                        <label for="logInEmail">Email address</label>
                        <input type="email" class="form-control" id="logInEmail" aria-describedby="emailHelp"
                               name="logInEmail">
                        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                    </div>
                    <div class="form-group">
                        <label for="logInPwd">Password</label>
                        <input type="password" class="form-control" id="logInPwd" name="logInPwd">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="exampleCheck1">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Log In</button>
                </form>
            </div>
        </td>
        <td>
            <div>
                <c:if test="${userAlreadyExists}">
                    <p>USER ALREADY EXISTS</p>
                </c:if>
                <form method="post" action="signup">
                    <div class="form-group">
                        <label for="SungUpEmail">Email address</label>
                        <input type="email" class="form-control" id="SungUpEmail" aria-describedby="emailHelp"
                               name="SungUpEmail">
                        <small id="signUpEmail" class="form-text text-muted">We'll never share your email with anyone
                            else.</small>
                    </div>
                    <div class="form-group">
                        <label for="signUpPwd">Password</label>
                        <input type="password" class="form-control" id="signUpPwd" name="signUpPwd">
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="signUpCheck">
                        <label class="form-check-label" for="exampleCheck1">Check me out</label>
                    </div>
                    <button type="submit" class="btn btn-primary">Sign Up</button>
                </form>
            </div>
        </td>
    </tr>

</table>


</body>
</html>
